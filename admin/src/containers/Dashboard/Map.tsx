import React, { useState } from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Polyline,
} from "react-google-maps";
import { compose, withStateHandlers, withProps } from "recompose";
import MapCard from "../../components/MapCard/MapCard";

const Map = compose(
  withStateHandlers(
    () => ({
      isOpen: false,
      // tslint:disable-next-line:align
    }),
    {
      onToggleOpen: ({ isOpen }) => () => ({
        isOpen: !isOpen,
      }),
    }
  ),
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyCl5BLxUS-xQnv2UCuBIPtc5B83_Uozhbk&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {
  const [progress, setProgress] = useState([]);
  return (
    <div>
      <GoogleMap
        defaultZoom={14}
        defaultCenter={{ lat: -1.2884, lng: 36.8233 }}
        defaultOptions={{
          streetViewControl: true,
          scaleControl: false,
          mapTypeControl: false,
          panControl: false,
          zoomControl: false,
          rotateControl: false,
          fullscreenControl: false,
          disableDefaultUI: true,
          scrollwheel: false,
        }}
      >
        {props.isMarkerShown && (
          <>
            <Marker position={progress[progress.length - 1]} />
            <Polyline path={progress} options={{ strokeColor: "#FF0000 " }} />
            <MapCard />
          </>
        )}
      </GoogleMap>
    </div>
  );
});

export default Map;
