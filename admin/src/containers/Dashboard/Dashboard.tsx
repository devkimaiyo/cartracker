import React from "react";
import Map from "./Map";
import { Mappage } from "./Dashboard.style";

function Dashboard() {
  return (
    <>
      <Mappage>
        <Map isMarkerShown />
      </Mappage>
    </>
  );
}

export default Dashboard;
