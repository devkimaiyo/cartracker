import React, { useEffect, useState } from "react";
import {
  OpenTrack,
  Icon,
  Track,
  Headerpanel,
  NavIcon,
  Title,
  Closepanel,
  PanelContent,
  TrackInfo,
  TrackDetails,
  Det,
  DetName,
  StartTime,
  OpenSeat,
  CarSeat,
  Car,
  Seats,
  SeatContent,
  SeatDetails,
  Driver,
  SeatTrack,
  Span,
} from "./MapCard.style";
import SeatPicker from "react-seat-picker";
import SockJS from "sockjs-client"; // Note this line
import Stomp from "stompjs";
import Geocode from "react-geocode";
const MapCard = ({ data, totalText }: any) => {
  let stompClient = null;
  const [isOpened, setIsOpened] = useState(false);
  const [isSeatOpened, setIsSeatOpened] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isSelected, setIsSelected] = useState(true);
  const [aIoId, setAIoId] = useState();
  const [state, setState] = useState();
  const [date, setDate] = useState();
  const [type, setType] = useState();
  const [longitudeInteger, setLongitudeInteger] = useState("");
  const [longitudeDecimal, setLongitudeDecimal] = useState("");
  const [latitudeInteger, setLatitudeInteger] = useState("");
  const [latitudeDecimal, setLatitudeDecimal] = useState("");
  const [location, setLocation] = useState();
  const [seats, setSeats] = useState([]);
  if (seats) var seat1 = seats.filter((obj) => obj.seatCode === "B2")[0];
  // console.log(seat1);
  function findLocation() {
    Geocode.setApiKey("AIzaSyAVJcpmZRCT43E3XahyoEIfDZ6PbJaRh6o");
    Geocode.setLanguage("en");
    Geocode.setRegion("es");
    Geocode.setLocationType("ROOFTOP");
    Geocode.enableDebug();
    Geocode.fromLatLng(
      `${latitudeInteger}` + "." + `${latitudeDecimal}`,
      `${longitudeInteger}` + "." + `${longitudeDecimal}`
    ).then(
      (response) => {
        const address = response.results[0].formatted_address;
        setLocation(address);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  useEffect(() => {
    const socket = new SockJS("https://trackersapi.herokuapp.com/websocket");
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function () {
      stompClient.subscribe("/topic/equipment", function (data) {
        setAIoId(JSON.parse(data.body).aIoId);
        setState(JSON.parse(data.body).state);
        setDate(JSON.parse(data.body).dt);
      });
      stompClient.subscribe("/topic/seat", function (data) {
        setType(JSON.parse(data.body).type);
        setSeats(JSON.parse(data.body).seats);

        console.log(JSON.parse(data.body).seats);
      });
      stompClient.subscribe("/topic/gps", function (data) {
        setLongitudeInteger(JSON.parse(data.body).longitudeInteger);
        setLongitudeDecimal(JSON.parse(data.body).longitudeDecimal);
        setLatitudeInteger(JSON.parse(data.body).latitudeInteger);
        setLatitudeDecimal(JSON.parse(data.body).latitudeDecimal);
      });
      // stompClient.subscribe("/topic/unknown", function (data) {
      //   console.log(JSON.parse(data.body));
      // });
    });
    if (longitudeInteger && latitudeInteger) {
      findLocation();
    }
    if (seats) getByValue(seats, "B2");

    return () => {};
  }, [longitudeInteger, latitudeInteger, seats]);

  function getByValue(arr, value) {
    for (var i = 0, iLen = arr.length; i < iLen; i++) {
      if (arr[i].b == value) console.log(arr[i]);
    }
  }

  function seatCounter(inputs: any) {
    if (seats) {
      let counter = 0;
      inputs.forEach((e) => {
        if (e["id"] != "") {
          counter++;
        }
      });
      return counter;
    }
  }

  function toggleSeat() {
    setIsSeatOpened(true);
    setIsOpened(false);
  }
  function toggle() {
    setIsOpened(true);
    setIsSeatOpened(false);
  }
  const addSeatCallback = async ({ row, number, id }, addCb) => {
    setLoading(true);

    console.log(`Added seat ${number}, row ${row}, id ${id}`);
    const newTooltip = `tooltip for id-${id} added by callback`;
    addCb(row, number, id, newTooltip);
    setLoading(false);
  };
  const removeSeatCallback = async ({ row, number, id }, removeCb) => {
    setLoading(true);
    await new Promise((resolve) => setTimeout(resolve, 100));
    console.log(`Removed seat ${number}, row ${row}, id ${id}`);
    const newTooltip = ["A", "B", "C"].includes(row) ? null : "";
    removeCb(row, number, newTooltip);
    setLoading(false);
  };

  const rows = [
    [
      null,
      null,
      { id: 1, number: 1, isSelected: isSelected, tooltip: "Cost: 15$" },
      { id: 2, number: 2, tooltip: "Cost: 15$" },
    ],
    [
      { id: 3, number: 3, tooltip: "Cost: 15$" },
      null,
      { id: 4, number: 4, tooltip: "Cost: 15$" },

      {
        id: 5,
        number: 5,
        tooltip: "Cost: 15$",
      },
    ],
    [
      {
        id: 6,
        number: 6,
        tooltip: "Cost: 15$",
      },
      null,
      { id: 7, number: 7, tooltip: "Cost: 15$" },
      { id: 8, number: 8, tooltip: "Cost: 15$" },
    ],
    [
      { id: 9, number: 9 },
      null,
      { id: 10, number: 10, tooltip: "Cost: 15$" },
      { id: 11, number: 11, tooltip: "Cost: 15$" },
    ],
    [
      { id: 12, number: 12, tooltip: "Cost: 15$" },
      null,
      { id: 13, number: 13, tooltip: "Cost: 15$" },
      { id: 14, number: 14, tooltip: "Cost: 15$" },
    ],
    [
      { id: 15, number: 15, tooltip: "Cost: 15$" },
      null,
      { id: 16, number: 16, tooltip: "Cost: 15$" },
      { id: 17, number: 17, tooltip: "Cost: 15$" },
    ],
    [
      { id: 18, number: 18, tooltip: "Cost: 15$" },
      null,
      { id: 19, number: 19, tooltip: "Cost: 15$" },
      { id: 20, number: 20, tooltip: "Cost: 15$" },
    ],
    [
      { id: 21, number: 21, tooltip: "Cost: 15$" },
      null,
      { id: 22, number: 22, tooltip: "Cost: 15$" },
      { id: 23, number: 23, tooltip: "Cost: 15$" },
    ],
    [
      { id: 24, number: 24, tooltip: "Cost: 15$" },
      null,
      { id: 25, number: 25, tooltip: "Cost: 15$" },
      { id: 26, number: 26, tooltip: "Cost: 15$" },
    ],
    [
      { id: 27, number: 27, tooltip: "Cost: 15$" },
      null,
      { id: 28, number: 28, tooltip: "Cost: 15$" },
      { id: 29, number: 30, tooltip: "Cost: 15$" },
    ],
  ];

  return (
    <>
      <OpenTrack onClick={toggle}>
        <Icon className="fas fa-route"></Icon>
      </OpenTrack>
      <OpenSeat onClick={toggleSeat}>
        <CarSeat></CarSeat>
      </OpenSeat>
      {isOpened && (
        <Track>
          <Headerpanel>
            <NavIcon>
              <i className="fas fa-route" />
            </NavIcon>
            <Title />
            <Closepanel
              onClick={() => {
                setIsOpened(false);
              }}
            >
              <i className="fas fa-times" />
            </Closepanel>
          </Headerpanel>

          <PanelContent>
            <TrackInfo>
              <TrackDetails>
                <Det>
                  <DetName>
                    <Span>Gateway ID:</Span> {aIoId}
                  </DetName>
                  <StartTime />
                </Det>
                <Det>
                  <DetName>
                    <Span>Online status:</Span>{" "}
                    {state === 1 ? "Online" : "Offline"}
                  </DetName>
                  <StartTime />
                </Det>
                <Det>
                  <DetName>
                    <Span>Latest location: </Span>
                    {location}
                  </DetName>
                  <StartTime />
                </Det>
                <Det>
                  <DetName>
                    <Span>The seat status is updated on:</Span> {date}
                  </DetName>
                  <StartTime />
                </Det>
                <Det>
                  <DetName>
                    <Span>Number of seats:</Span> {seatCounter(seats)}
                  </DetName>
                  <StartTime />
                </Det>
              </TrackDetails>
            </TrackInfo>
          </PanelContent>
        </Track>
      )}
      {isSeatOpened && (
        <div>
          <SeatTrack>
            <Headerpanel>
              <NavIcon>
                <i className="fas fa-route" />
              </NavIcon>
              <Title />
              <Closepanel
                onClick={() => {
                  setIsSeatOpened(false);
                }}
              >
                <i className="fas fa-times" />
              </Closepanel>
            </Headerpanel>

            <SeatContent>
              <SeatDetails>
                <Car style={{ width: 200 }}>
                  <Seats>
                    <Driver />
                    <div style={{ marginTop: "50px", marginBottom: "10px" }}>
                      <SeatPicker
                        addSeatCallback={addSeatCallback}
                        removeSeatCallback={removeSeatCallback}
                        rows={rows}
                        maxReservableSeats={30}
                        visible
                        selectedByDefault
                        loading={loading}
                        tooltipProps={{ multiline: true }}
                      />
                    </div>
                  </Seats>
                </Car>
              </SeatDetails>
            </SeatContent>
          </SeatTrack>
        </div>
      )}
    </>
  );
};

export default MapCard;
