import { styled } from "baseui";
import { GiCarSeat } from "react-icons/gi";
import car from "../../image/car.png";
import driver from "../../image/driver1.png";
export const Mappage = styled("div", () => ({
  position: "relative",
  width: "100%",
  height: "calc(100vh - 56px)",
  zIndex: 1,
}));

export const OpenTrack = styled("div", ({ $theme }) => ({
  position: "absolute",
  top: "70px",
  right: "6px",
  width: "50px",
  height: " 50px",
  borderRadius: "50%",
  background: "#fff",
  boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.2)",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  cursor: "pointer",

  "@media  only screen and (max-width: 767px)": {
    padding: "20px 0",
    // alignItems: 'flex-start',
  },
}));
export const OpenSeat = styled("div", ({ $theme }) => ({
  position: "absolute",
  top: "140px",
  right: "6px",
  width: "50px",
  height: " 50px",
  borderRadius: "50%",
  background: "#fff",
  boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.2)",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  cursor: "pointer",

  "@media  only screen and (max-width: 767px)": {
    padding: "20px 0",
    // alignItems: 'flex-start',
  },
}));

export const Icon = styled("i", ({ $theme }) => ({
  display: "flex",
  alignItems: "center",
  margin: "0 auto",
  opacity: "0.7",
  transition: "transform .2s",
}));
export const CarSeat = styled(GiCarSeat, ({ $theme }) => ({
  display: "flex",
  alignItems: "center",
  margin: "0 auto",
  opacity: "0.7",
  transition: "transform .2s",
}));
export const Track = styled("div", ({ $theme }) => ({
  position: "absolute",
  top: "10px",
  right: "60px",
  width: "500px",
  background: "#fff",
  boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.2)",
  borderRadius: "2px",
  overflow: "hidden",
}));
export const Headerpanel = styled("div", ({ $theme }) => ({
  display: "flex",
  alignItems: "center",
  borderBottom: "1px solid #ddd",
  boxShadow: "0px 0px 3px 0px rgba(0,0,0,0.2)",
  color: $theme.colors.textNormal,
}));
export const NavIcon = styled("div", ({ $theme }) => ({
  fontSize: "16px",
  width: "44px",
  textAlign: "center",
}));
export const Title = styled("div", ({ $theme }) => ({
  flex: "1",
  fontSize: "16px",
  lineHeight: "1.2",
}));
export const Closepanel = styled("div", ({ $theme }) => ({
  padding: "15px",
  fontSize: " 16px",
  lineHeight: "1",
  cursor: "pointer",
  opacity: "0.7",
  transition: "transform .2s",
  ":hover": {
    opacity: "1",
    transform: "scale(1.2)",
  },
}));
export const PanelContent = styled("div", ({ $theme }) => ({
  maxHeight: "calc(100vh - 56px - 10px - 56px - 20px - 40px)",
  overflow: "hidden",
  display: "flex",
  width: "100%",
  transition: "all 0.4s",
}));
export const TrackInfo = styled("div", ({ $theme }) => ({
  flex: "1",
  overflowY: "auto",
  overflowX: "hidden",
  borderBottom: "6px solid #e84141",
}));
export const TrackDetails = styled("div", ({ $theme }) => ({
  display: "flex",
  flexDirection: "column",
  marginBottom: "15px",
}));
export const Det = styled("div", ({ $theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: "12px 0",
  borderBottom: "1px solid rgba(0,0,0,0.06)",
}));
export const DetName = styled("div", ({ $theme }) => ({
  width: "auto",
  padding: "0 15px",
  fontSize: " 14px",
  color: "#333",
  textAlign: "right",
  lineHeight: "1.2",
}));
export const StartTime = styled("div", ({ $theme }) => ({
  flex: "1",
  fontSize: "14px",
  color: "#333",
  lineHeight: "1.2",
}));
export const Tracklink = styled("div", ({ $theme }) => ({
  width: "100%",
  display: "flex",
  textAlign: "left",
}));
export const Span = styled("span", ({ $theme }) => ({
  color: $theme.colors.textNormal,
}));
export const Breadcrumbs = styled("div", ({ $theme }) => ({
  position: "absolute",
  bottom: "24px",
  right: "60px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  opacity: "0",
}));
export const Breadcrumbswrap = styled("div", ({ $theme }) => ({
  background: " #fff",
  boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.2)",
  borderRadius: "2px",
  overflow: "hidden",
  lineHeight: " 1",
  padding: "10px 15px",
  fontSize: "14px",
}));
export const SeatTrack = styled("div", ({ $theme }) => ({
  position: "absolute",
  top: "10px",
  right: "60px",
  width: "215px",
  background: "#fff",
  boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.2)",
  borderRadius: "2px",
  overflow: "hidden",
}));
export const Car = styled("div", ({ $theme }) => ({
  background: `url(${car})`,
  backgroundSize: "97% 100%",
  backgroundRepeat: "no-repeat",
  width: "150px",
  margin: "0 auto",
}));
export const Seats = styled("div", ({ $theme }) => ({
  padding: "18px 13px",
  fontSize: "8px",
}));
export const Driver = styled("div", ({ $theme }) => ({
  background: `url(${driver})`,
  width: "50px",
  height: "50px",
  float: "right",
  marginRight: "30px",
  backgroundSize: "100% 100%",
  backgroundRepeat: " no-repeat",
}));
export const SeatContent = styled("div", ({ $theme }) => ({
  maxHeight: "calc(100vh - 56px - 10px - 56px - 20px - 40px)",
  overflow: "hidden",
  display: "flex",
  width: "100%",
  transition: "all 0.4s",
}));
export const SeatDetails = styled("div", ({ $theme }) => ({
  display: "flex",
  flexDirection: "column",
  marginLeft: "10px",
  marginRight: "10px",
}));
export const Zfx = styled("div", ({ $theme }) => ({
  margin: "5px",
}));
export const Myred = styled("div", ({ $theme }) => ({
  width: "15px",
  height: "15px",
  background: "#FF0000",
  float: "left",
  marginRight: "8px",
}));
