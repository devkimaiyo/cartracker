// **************** ROUTE CONSTANT START **************************
// General Page Section
export const DASHBOARD = "/";
export const LOGIN = "/login";
export const LOGOUT = "/logout";

// **************** ROUTE CONSTANT END **************************

export const CURRENCY = "$";
